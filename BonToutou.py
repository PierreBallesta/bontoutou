#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 11:33:49 2020

@author: Pierre

This is the model part of the P6 program of the Machine Learning Engineer courses of OpenClassrooms.
"""

## tk.keras dependencies.
from tensorflow.keras.applications import Xception
from tensorflow.keras.layers import Dense, Flatten, Dropout, InputLayer, \
GlobalAveragePooling2D, BatchNormalization, Activation, Lambda
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import callbacks
from tensorflow.keras import  optimizers
from tensorflow.keras.utils import to_categorical
## Other libraries.
import numpy as np
from PIL import Image
import pickle
# Homemade
import imagetreat as it

# Size of the images :
im_size = 299
# Some callback functions.
def scheduler(epoch, lr):
    # decrease by a factor 10 every 10 epochs.
    return lr/1.1

# Save the training loss and accuracy for each mini-batches.
class Histories(callbacks.Callback):

    def on_train_begin(self,logs={}):
        self.losses = []
        self.accuracies = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        self.accuracies.append(logs.get('acc'))

# Load the dictionnary of races:
with open('dog_dictionnary.pkl', 'rb') as f:
        dico= pickle.load(f)
rev_dico ={dico[a]:a for a in dico}

# Makes square images of the good size.
def getsquare(image):
    b=[0,0,0,0,0]
    vect = np.zeros((len(image),299,299,3))
    k=0
    for i in image:
        img = Image.open(i)
        img = it.randomtransform(img,b)
        img = it.reducing_image(img,im_size)
        vect[k,:,:,:]=img
        k+=1
    return vect

# Name of the model for save and load :
SAVED_AS = 'modifiedXception_120_top2.hdf5'

# We load the Xception model.
xception = Xception(include_top=False,weights="imagenet",input_tensor=None,
    input_shape=(299, 299, 3))

# We add a global average pooling.
model = Sequential()
model.add(xception)
model.add(GlobalAveragePooling2D())

# We create modified the model to transform images into a 2048 vectors.
imtransform_model = Sequential()
imtransform_model.add(model)

def imtransform(image):
    vect = getsquare(image)
    vect = imtransform_model.predict(vect)
    return vect

# We load the model to detect the race of the dog.
race_model = load_model(SAVED_AS)

def bontoutou_classify(vector):
    races = race_model.predict(vector)
    races = np.argmax(races, axis=1)
    return races

def bontoutou_train(im_path=[],im_labels=[],save_as=SAVED_AS):
    im_vect = imtransform(im_path)
    # Load pretransformed data.
    with open('X0_train.p', 'rb') as f:
        vect = pickle.load(f)
    with open('Y0_train.p', 'rb') as f:
        labels = pickle.load(f)
    # Add the new data.
    # Check that the data is not already present.
    ind=[i for i in range(im_vect.shape[0]) if not 
         any(all(im_vect[i,:]==vect[j,:]) for j in range(vect.shape[0]))]
    if ind:
        vect=np.vstack([vect,im_vect[ind,:]])
        labels=np.vstack([labels,
                      to_categorical(im_labels[ind],num_classes=120)])
    else : 
        print('Images are already in the database.')
        return
    # Creating the weigths:
    c = np.sum(labels,axis=0)
    A=1/np.mean(1/c)
    c_weight={i:A/c[i] for i in range(len(c))}
    # Save the data.
    pickle.dump(vect, open( "X0_train.p", "wb" ))
    pickle.dump(labels, open( "Y0_train.p", "wb" ))
    histories = Histories()
    schedule = callbacks.LearningRateScheduler(scheduler)
    mycallback = [histories,schedule]
    # Fit the model.
    race_model.compile(loss="categorical_crossentropy", 
                  optimizer=optimizers.Adam(lr=0.005, epsilon=0.000000001), metrics=["accuracy"])
    race_model.fit(x=vect,y=labels, batch_size=512,class_weight=c_weight,epochs=10,
                   verbose=1, workers=3, callbacks=mycallback)
    # Save the model.
    race_model.save(SAVED_AS)
    
