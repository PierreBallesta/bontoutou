# BonToutou : Dog classifier.
Based on the Xception architecture, trained on the [Stanford Dogs Dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/).

## Installing Anaconda.
To install [Anaconda](https://docs.anaconda.com/anaconda/install/), follow the instructions in the link. 

## Creating an environment.
You can create an environment by typing in the console:  
```
python -m venv myenv
```  
Once the environment is created, you have to activate it:  
```
source myenv/bin/activate
```  
You can then install the needed dependencies:  
```
pip install -r requirements.txt
```  
Where "requirements.txt" is the document contained in the classifier files.  
Finally, type the following two lines of code in your console:   
```
pip install --user ipykernel
```  
This will install the package to allow for Jupyter to use your environment.  
Then type the following:  
```
python -m ipykernel install --user --name=myenv
```  
And you're good to go.

## Classifying an image or a serie of images.
Open a Jupyter notebook, you can either use the one provided with the classifier or a new one.  
On the Jupyter menu bar, click on **Kernel** and choose your environment **myenv**.  
If you're using a new notebook copy paste the following in the first tab to install the needed libraries:    
```
import BonToutou as bt  
```  
Create a list containing the location of your images *e.g.* "mylist". Then call the following function.  
```
vect = bt.imtransform(mylist)  
```
```
dog_class = bt.bontoutou_classify(vect)  
```  
You can access a the races with:  
```
races=[bt.rev_dico[i] for i in dog_class] 
```  
``` 
print(races)
```
## Training the network further.
If you have images and races you're certain of, you can train the network further.  
First check ```bt.dico``` to get the labels of each races.  
Create a list of link to the images ```mylist``` and a corresponding list of labels ```mylabels```.  
In your notebook run the following:
```
bt.bontoutou_train(im_path=mylist,im_labels=mylabels)
```  
This will update your network with the new images.