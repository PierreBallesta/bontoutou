#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 11:33:49 2020

@author: Pierre

This is the images treatment part of the P6 program of the Machine Learning Engineer courses of OpenClassrooms.
"""
from skimage import transform, exposure
import numpy as np
from numpy import asarray
import random

# We define some transformations.

def centering(image):
    h,w,c = image.shape
    m_length = min(h,w)
    hc = int(np.floor((h-m_length)/2))
    wc = int(np.floor((w-m_length)/2))
    img = image[hc:hc+m_length-1,wc:wc+m_length-1,:]
    return img

def translationL(image):
    h,w,c = image.shape
    m_length = min(h,w)
    img = image[0:m_length-1,0:m_length-1,:]
    return img

def translationR(image):
    h,w,c = image.shape
    m_length = min(h,w)
    img = image[h-m_length:,w-m_length:,:]
    return img
 
def rotation(image,angle):
    h,w,c = image.shape
    img = transform.rotate(image, angle=angle, resize=False, cval=0.5)
    return img

def mirror(image):
    img = image[:, ::-1]
    return img

def zoom(image,factor):
    h,w,c = image.shape
    h_zoom = random.randint(0, int((h-1)*(1-factor)))
    w_zoom = random.randint(0, int((w-1)*(1-factor)))
    img = image[h_zoom:h_zoom+int((h-1)*factor),
                w_zoom:w_zoom+int((w-1)*factor),:]
    return img

def equalize(img_da):
    # equalize
    img_eq = np.zeros(img_da.shape)
    for channel in range(img_da.shape[2]):
        img_eq[:, :, channel] = exposure.equalize_hist(img_da[:, :, channel])*2-1 
    return img_eq

def reducing_image(img_da,n):
    # centering
    h,w,c = img_da.shape
    m_length = min(h,w)
    h2 = int((h-m_length)/2)
    w2 = int((w-m_length)/2)
    img_eq = img_da[h2:h2+m_length-1,w2:w2+m_length-1,:]
    # reducing
    img_eq = transform.resize(img_eq, (n, n), anti_aliasing=False)
    return img_eq

def make_BW(img_da,proba=0):
    img = np.zeros(img_da.shape)
    a=random.uniform(0, 1)
    if a < proba:
        imBW = 0.33*img_da[:,:,0]+0.34*img_da[:,:,1]+0.33*img_da[:,:,2]
        for i in range(img_da.shape[2]):
            img[:,:,i] = imBW
    else:
        img = img_da
    return img

# Create the random variable.

def transvariables():
    b = [random.randint(0, 1),
            random.randint(0, 1),
            random.randint(0, 1),
            random.randint(0, 2),
            max(random.randint(-1, 1),0)]
    return b

# We create a random transformation sequence.
def randomtransform(image,variables):
    img = asarray(image)
    img = equalize(img)
    b=variables
    # 0.5 chances of mirroring.
    if b[0] == 1:
        img = mirror(img)
    # 0.5 chances of zooming.
    if b[1] == 1:
        factor = random.uniform(0.35, 0.99)
        img = zoom(img,factor)
    # 0.5 chances of rotation.
    if b[2] == 1:
        angle = [random.randint(1, 179),random.randint(181, 359)][random.randint(0, 1)]
        img = rotation(img,angle)
    # 2/3 chances of translation.
    if b[3] == 1 and img.shape[0] != img.shape[1]:
        img = translationL(img)
    elif b[3] == 2 and img.shape[0] != img.shape[1]:
        img = translationR(img)
    # 1/3 chances of black and white.
    if b[4] == 1:
        imBW = 0.33*img[:,:,0]+0.34*img[:,:,1]+0.33*img[:,:,2]
        for i in range(img.shape[2]):
            img[:,:,i] = imBW
     # getting a centered square image.
    img = centering(img)
    return img
    
